# Frontend Developer Interview Project

## Description

Unfortunately, I was not able to retrive stock values from Alpha Vantage, because I could only make 5 calls per minute.
In order to solve this, a fake API was created with json-server in order to retrieve stock values. Unfortunately, The api that I created does not accept a stock symbol parameter, so it only represents one stock value that I used for all the stocks in the app.

I hosted it in Heroku, so you can see the response here: https://stock-fake-api.herokuapp.com/TimeSeries5min

Alpha Vantage response:

```
{
  "Note": "Thank you for using Alpha Vantage! Our standard API call frequency is 5 calls per minute and 500 calls per day. Please visit https://www.alphavantage.co/premium/ if you would like to target a higher API call frequency."
}
```

## Features

Project was built using the Expo CLI.

- [x] ReactNative with Expo CLI
- [x] react navigation, @react-navigation/drawer
- [x] AsyncStorage (@react-native-async-storage/async-storage)
- [x] axios

## Run locally

```
# Clone or download repository

# Install dependencies
yarn install

# Start a Webpack dev server for the web app
yarn web

# Run the Android app binary locally
yarn android

# Run the iOS app binary locally
yarn ios

# Start a local dev server
yarn start
```
