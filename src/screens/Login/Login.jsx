import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, View, Pressable, Switch } from 'react-native';
import { useEffect } from 'react';
import { getData, storeData } from '../../services/storage';
import PropTypes from 'prop-types';

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [saveEnabled, setSaveEnabled] = useState(false);

  const handleLogin = () => {
    if (saveEnabled) storeData('email', email);
    navigation.navigate('Drawer', { email });
  };

  useEffect(() => {
    getData('email').then((res) => {
      setEmail(res);
    });
  }, [getData]);

  return (
    <View style={styles.wrapperContainer}>
      <View style={styles.container}>
        <View style={styles.topWrapper}>
          <Text style={styles.title}>Log In</Text>

          <Text style={styles.emailText}>Email</Text>
          <TextInput
            style={styles.emailInput}
            placeholder="Enter email"
            keyboardType="email-address"
            value={email}
            onChangeText={(newText) => setEmail(newText)}
          />
          <View style={styles.switchSection}>
            <Text style={styles.switchText}>Remember Username</Text>
            <Switch
              trackColor="#767577"
              thumbColor={saveEnabled ? 'rgb(42, 255, 170)' : 'rgb(65, 65, 67)'}
              onValueChange={setSaveEnabled}
              value={saveEnabled}
            />
          </View>
        </View>

        <View style={styles.bottomWrapper}>
          <Pressable title="Log in" style={styles.button} onPress={handleLogin}>
            <Text style={styles.buttonText}>Log in</Text>
          </Pressable>
        </View>
      </View>
    </View>
  );
};

LoginScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  wrapperContainer: {
    width: '100vw',
    height: '100%',
  },
  container: {
    margin: '3rem',
    textAlign: 'center',
    flex: 1,
    justifyContent: 'space-between',
    alignContent: 'center',
    textAlign: 'center',
    marginTop: '4rem',
    marginBottom: '2rem',
  },
  topWrapper: {},
  bottomWrapper: {},
  title: {
    fontSize: '1.5rem',
    fontWeight: 700,
    marginBottom: '3rem',
  },
  emailInput: {
    height: 40,
    maxHeight: 40,
    borderBottomColor: 'rgb(65, 65, 67)',
    borderBottomWidth: 2,
    marginBottom: '2rem',
  },
  button: {
    height: '56px',
    borderRadius: '3rem',
    backgroundColor: 'rgb(42, 255, 170)',
    padding: '1rem',
    textAlign: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'rgb(65, 65, 67)',
    textAlign: 'center',
    fontSize: '1rem',
    fontWeight: '700',
  },
  switchSection: {
    maxHeight: '40px',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  switchText: {
    fontSize: '1rem',
    fontWeight: 700,
    lineHeight: '32px',
  },
  emailText: {
    fontWeight: 700,
    fontSize: '1rem',
    textAlign: 'left',
  },
});

export default LoginScreen;
