import { StyleSheet, View } from 'react-native';
import PositionItem from './PositionItem';
import { useState, useEffect } from 'react';
import { getPositions } from '../../services/home';
import PropTypes from 'prop-types';
import PositionList from './PositionList';

const AccountOverviewScreen = ({ route }) => {
  const [positions, setPositions] = useState([]);
  const [claimed, setClaimed] = useState([]);
  const [unclaimed, setUnlaimed] = useState([]);
  const renderItem = ({ item }) => (
    <PositionItem
      name={item.name}
      brandName={item.brand_name}
      symbol={item.symbol}
      style={styles.list}
      amount={item.amount}
    />
  );

  useEffect(() => {
    setPositions(getPositions(route.params.email));
  }, []);

  useEffect(() => {
    const { claimed, unclaimed } = positions.reduce(
      (result, position) => {
        if (position.type === 'CLAIMED') result.claimed.push(position);
        else result.unclaimed.push(position);
        return result;
      },
      { claimed: [], unclaimed: [] }
    );
    setClaimed(claimed);
    setUnlaimed(unclaimed);
  }, [positions]);

  return (
    <View style={styles.container}>
      <PositionList category="Claimed Positions" positions={claimed} />
      <PositionList category="Unclaimed Positions" positions={unclaimed} />
    </View>
  );
};

AccountOverviewScreen.propTypes = {
  route: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: '100%',
  },
});

export default AccountOverviewScreen;
