import { StyleSheet, Text, View, SafeAreaView, FlatList } from 'react-native';
import PositionItem from './PositionItem';
import PropTypes from 'prop-types';

const PositionList = ({ category, positions }) => {
  const renderItem = ({ item }) => (
    <PositionItem
      name={item.name}
      brandName={item.brand_name}
      symbol={item.symbol}
      style={styles.list}
      amount={item.amount}
    />
  );

  return (
    <View>
      <Text style={styles.category}>{category}</Text>
      <SafeAreaView style={styles.lists}>
        <FlatList data={positions} renderItem={renderItem} keyExtractor={(item) => item.brand} />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  lists: {
    margin: 0,
    width: '100%',
    marginBottom: '1rem',
  },
  list: {
    paddingHorizontal: '0.3rem',
  },
  category: {
    fontSize: '1rem',
    fontWeight: 700,
    width: '100%',
    marginLeft: '0.8rem',
    marginTop: '0.5rem',
    marginBottom: '0.2rem',
  },
});

PositionList.propTypes = {
  category: PropTypes.string,
  positions: PropTypes.array,
};

export default PositionList;
