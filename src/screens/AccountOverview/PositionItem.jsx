import { StyleSheet, Text, View } from 'react-native';
import { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { getFakeStockPrice } from '../../services/fakeStockServer';

const PositionItem = ({ brandName, symbol, amount }) => {
  const [croppedAmount, setCroppedAmount] = useState('');
  const [stockValue, setStockValue] = useState(0);

  useEffect(() => {
    getFakeStockPrice(symbol).then((res) => {
      setStockValue(res);
    });
  }, [getFakeStockPrice, symbol]);

  useEffect(() => {
    setCroppedAmount(amount.slice(0, -1));
  }, []);

  const finalValue = useMemo(() => {
    return (stockValue * amount).toFixed(2);
  }, [stockValue, amount]);

  return (
    <View style={styles.listItem}>
      <View style={styles.listItemText}>
        <Text style={styles.listItemTitle}>{symbol}</Text>
        <Text style={styles.listItemDescription}>{brandName}</Text>
      </View>
      <View style={styles.listItemValue}>
        <Text style={styles.listItemValue}>${finalValue}</Text>
        <Text style={styles.listItemShares}>{croppedAmount} shares</Text>
      </View>
    </View>
  );
};

PositionItem.propTypes = {
  brandName: PropTypes.string,
  symbol: PropTypes.string,
  amount: PropTypes.string,
};

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: '#fff',
    paddingHorizontal: '0.8rem',
    paddingVertical: '0.2rem',
    marginTop: 8,
    marginBottom: 0,
    borderRadius: '0.4rem',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  listItemText: {
    flex: 1,
  },
  listItemTitle: {
    fontSize: '1rem',
    fontWeight: 700,
  },
  listItemDescription: {
    color: 'rgb(130, 129, 129)',
  },
  listItemValue: {
    flex: 1,
    alignItems: 'flex-end',
    fontWeight: 700,
  },
  listItemShares: {
    color: 'rgb(130, 129, 129)',
  },
});

export default PositionItem;
