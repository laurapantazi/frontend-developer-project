import { StyleSheet, Text, View, SafeAreaView, FlatList, Pressable } from 'react-native';
import { useState, useEffect } from 'react';
import { getBrands, getPositions } from '../../services/home';
import BrandItem from './BrandItem';
import PropTypes from 'prop-types';
import { getFakeStockPrice } from '../../services/fakeStockServer';

const HomeScreen = ({ route, navigation }) => {
  const [email, setEmail] = useState('');
  const [brands, setBrands] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalInt, setTotalInt] = useState(0);
  const [totalDec, setTotalDec] = useState(0);
  const renderItem = ({ item }) => (
    <BrandItem email={email} name={item.name} percentBack={item.percent_back} symbol={item.symbol} />
  );

  useEffect(() => {
    const email = route.params.email;
    setEmail(email);
    setBrands(getBrands(email));

    const positions = getPositions(email);
    for (let i in positions) {
      getFakeStockPrice(positions[i].symbol).then((res) => {
        setTotalAmount((prevState) => prevState + positions[i].amount * res);
      });
    }
  }, []);

  useEffect(() => {
    const [int, dec] = totalAmount.toFixed(2).toString().split('.');
    setTotalInt(int);
    setTotalDec(dec);
  }, [totalAmount]);

  return (
    <View style={styles.container}>
      <Pressable style={styles.button} onPress={() => navigation.navigate('AccountOverview')}>
        <Text style={styles.buttonText}>ACCOUNT VALUE</Text>
        <Text style={styles.buttonValue}>
          ${totalInt}
          <Text style={styles.buttonValueDecimal}>.{totalDec}</Text>
        </Text>
      </Pressable>
      <Text style={styles.brandText}>Brands</Text>
      <SafeAreaView style={styles.lists}>
        <FlatList style={styles.list} data={brands} renderItem={renderItem} keyExtractor={(item) => item.id} />
      </SafeAreaView>
    </View>
  );
};

HomeScreen.propTypes = {
  route: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  lists: {
    flex: 1,
    margin: 0,
    width: '100%',
    backgroundColor: 'rgb(242, 242, 242)',
  },
  list: {
    paddingHorizontal: '0.3rem',
  },
  brandText: {
    fontSize: '1.2rem',
    fontWeight: 700,
    width: '100%',
    marginLeft: '1.4rem',
    marginBottom: '0.5rem',
  },
  button: {
    width: 180,
    height: 180,
    borderRadius: '100%',
    fontSize: '1rem',
    color: '#000',
    textAlign: 'center',
    border: '4px solid rgb(42, 255, 170)',
    marginBottom: '1rem',
    position: 'relative',
  },
  buttonText: {
    textAlign: 'center',
    position: 'absolute',
    top: '20px',
    left: '35px',
    fontSize: '0.75rem',
    color: '#000',
  },
  buttonValue: {
    position: 'absolute',
    top: '52%',
    left: '50%',
    transform: 'translateY(-50%) translateX(-50%)',
    color: '#000',
    fontSize: '2.4rem',
    fontWeight: 700,
  },
  buttonValueDecimal: {
    fontSize: '1.2rem',
    textAlignVertical: 'top',
  },
});

export default HomeScreen;
