import { StyleSheet, Text, View, Image, Pressable } from 'react-native';
import BrandLogo from '../../assets/PlaceholderLogo.png';
import { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import ModalNotification from '../../components/ModalNotification';
import { getFakeStockPrice } from '../../services/fakeStockServer';
import { getPosition } from '../../services/home';

const BrandItem = ({ email, name, percentBack, symbol }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [stockValue, setStockValue] = useState(0);
  const [amount, setAmount] = useState(0);

  useEffect(() => {
    setAmount(getPosition(email, symbol, name));
    getFakeStockPrice(symbol).then((res) => {
      setStockValue(res);
    });
  }, [getPosition]);

  const finalValue = useMemo(() => {
    return (stockValue * amount).toFixed(2);
  }, [stockValue, amount]);
  return (
    <>
      <Pressable style={styles.listItem} onPress={() => setModalVisible(true)}>
        <Image style={styles.listItemLogo} source={BrandLogo} />
        <View style={styles.listItemText}>
          <Text style={styles.listItemTitle}>{name}</Text>
          <Text style={styles.listItemDescription}>
            {percentBack}% back via {symbol}
          </Text>
        </View>
        <Text style={styles.listItemValue}>${finalValue}</Text>
      </Pressable>

      <ModalNotification name={name} modalVisible={modalVisible} setModalVisible={setModalVisible} />
    </>
  );
};

BrandItem.propTypes = {
  email: PropTypes.string,
  name: PropTypes.string,
  percentBack: PropTypes.string,
  symbol: PropTypes.string,
};

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: '#fff',
    padding: '1rem',
    marginTop: 8,
    marginBottom: 0,
    borderRadius: '0.4rem',
    flex: 1,
    flexDirection: 'row',
  },
  listItemText: {
    flex: 1,
    alignSelf: 'center',
  },
  listItemLogo: {
    width: 40,
    height: 40,
    borderRadius: '100%',
    marginRight: '0.8rem',
  },
  listItemTitle: {
    fontSize: '1rem',
    fontWeight: 700,
  },
  listItemDescription: {
    color: 'rgb(130, 129, 129)',
  },
  listItemValue: {
    fontSize: '1rem',
    alignSelf: 'center',
    fontWeight: 700,
  },
});

export default BrandItem;
