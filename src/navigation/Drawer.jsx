import 'react-native-gesture-handler';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from '../screens/Home/Home';
import AccountOverviewScreen from '../screens/AccountOverview/AccountOverview';
import { Button, StyleSheet } from 'react-native';
import { useState, useEffect } from 'react';
import { getFullName } from '../services/home';
import PropTypes from 'prop-types';
import DrawerTitle from '../components/DrawerTitle';

const DrawerScreen = ({ route, navigation }) => {
  const [fullName, setFullName] = useState('');
  const Drawer = createDrawerNavigator();

  useEffect(() => {
    setFullName(getFullName(route.params.email));
  }, [getFullName]);

  return (
    <Drawer.Navigator drawerContent={(props) => <DrawerTitle fullName={fullName} {...props} />} style={styles.nav}>
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        initialParams={{email: route.params.email}}
        options={{
          headerTitle: '',
          headerShadowVisible: false,
          drawerActiveTintColor: 'rgb(65, 65, 67)',
          drawerActiveBackgroundColor: 'rgb(42, 255, 170)',
        }}
      />
      <Drawer.Screen
        name="AccountOverview"
        component={AccountOverviewScreen}
        initialParams={{email: route.params.email}}
        options={{
          title: 'Account Overview',
          headerTitleAlign: 'center',
          headerShadowVisible: false,
          drawerActiveTintColor: 'rgb(65, 65, 67)',
          drawerActiveBackgroundColor: 'rgb(42, 255, 170)',
          headerTitle: 'Account Overview',
          headerRight: () => <Button title="Back" onPress={() => navigation.navigate('Home')}></Button>,
        }}
      />
    </Drawer.Navigator>
  );
};

DrawerScreen.propTypes = {
  route: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({});

export default DrawerScreen;
