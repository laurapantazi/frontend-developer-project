import 'react-native-gesture-handler';
import { StyleSheet, Text } from 'react-native';
import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import PropTypes from 'prop-types';

const DrawerTitle = (props) => {
  return (
    <DrawerContentScrollView {...props}>
      <Text style={styles.fullName}>{props.fullName}</Text>
      <DrawerItemList {...props} style={styles.list} />
    </DrawerContentScrollView>
  );
};

DrawerTitle.propTypes = {
  props: PropTypes.object,
};

const styles = StyleSheet.create({
  fullName: {
    color: 'rgb(65, 65, 67)',
    fontSize: '3rem',
    margin: '1rem',
    fontWeight: 700,
  },
  list: {
    margin: '1rem',
  },
});

export default DrawerTitle;
