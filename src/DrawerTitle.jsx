import { View, Text } from 'react-native';
import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';

const DrawerTitle = (props) => {
  return (
<>
     // 
     <DrawerContentScrollView {...props} contentContainerStyle={{ color: '#000', margin: '1rem' }}>
     <Text
       style={{
         color: '#000',
         fontSize: '3rem',
         margin: '1rem',
       }}
     >
       Ryan Gary
     </Text>
     <DrawerItemList {...props} style={{margin: '1rem'}} />
   </DrawerContentScrollView>


    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props} contentContainerStyle={{ color: '#000' }}>
        <Text
          style={{
            color: '#000',
            fontSize: '3rem',
            marginBottom: 5,
          }}
        >
          John Doe
        </Text>
        {/* <View style={{ flex: 1, backgroundColor: '#fff', paddingTop: 10, fontWeight: 700 }}> */}
        <DrawerItemList {...props} />
        {/* </View> */}
      </DrawerContentScrollView>
    </View>
    </>
  );
};

export default DrawerTitle;
