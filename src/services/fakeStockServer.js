import axios from 'axios';

export const getFakeStockPrice = async (symbol) => {
  try {
    const response = await axios.get(`https://stock-fake-api.herokuapp.com/TimeSeries5min`);

    const firstKey = Object.keys(response.data)[0];

    return response.data[firstKey]['4. close'];
  } catch (e) {
    console.log('ERROR', e);
  }
};
