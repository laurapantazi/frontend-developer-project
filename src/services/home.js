import HomeEndpointReturn from '../assets/HomeEndpointReturn.json';

const defaultUserEmail = 'ed.teach@bitsofstock.com';

export const getUser = (email) => {
  return HomeEndpointReturn[defaultUserEmail].user;
};

export const getBrands = (email) => {
  return HomeEndpointReturn[defaultUserEmail].brands;
};

export const getPositions = (email) => {
  return HomeEndpointReturn[defaultUserEmail].positions;
};

export const getFullName = (email) => {
  const { first_name, last_name } = HomeEndpointReturn[defaultUserEmail].user;
  return `${first_name} ${last_name}`;
};

export const getPosition = (email, symbol, name) => {
  const positions = getPositions(defaultUserEmail);
  for (let i in positions) {
    if (positions[i].symbol === symbol && positions[i].brand_name === name) return positions[i].amount;
  }
  return 0;
};
