import axios from 'axios';

export const getStockPrice = async (symbol) => {
  try {
    const response = await axios.get(
      `https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=${symbol}&interval=5min&apikey=${process.env.ALPHAVANTAGE_API_KEY}`
    );

    const firstKey = Object.keys(response.data['Time Series (5min)'])[0];
    return response.data?.['Time Series (5min)'][firstKey]['4. close'];
  } catch (e) {
    console.log('ERROR', e);
  }
};
